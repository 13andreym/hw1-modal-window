import React from 'react';
import Button from './components/Button/Button.jsx';
import Modal from './components/Modal/Modal.jsx';
import Modal_secondary from './components//Modal/Modal_secondary.jsx';
import './App.css';


export default class App extends React.Component {
  state = {
    isFirstModal: false,
    isSecondModal: false,
    isGreatModal: false,
    isFineModal: false,
    isSoSoModal: false
  };

  openFirstModal = () => {
    this.setState({ isFirstModal: true });
  };

  openSecondModal = () => {
    this.setState({ isSecondModal: true });
  };

  openGreatModal = () => {
    this.setState({ isGreatModal: true });
  }
  openFineModal = () => {
    this.setState({ isFineModal: true });
  }
  openSoSoModal = () => {
    this.setState({ isSoSoModal: true });
  }

  closeModal = () => {
    this.setState({ 
      isFirstModal: false,
      isSecondModal: false,
      isGreatModal: false,
      isFineModal: false,
      isSoSoModal: false
    });
  };

  handleOutside = (event) => {
    if (event.currentTarget === event.target) {
      this.closeModal();
    }
  };


  render() {
    return (
      <div className="btn-wrapper" >
        <Button
          optionalСlassName="btn-primary"
          backgroundColor="#8A33FD
"
          text="Open first modal"
          onClick={this.openFirstModal}
        />
        <Button
          optionalСlassName="btn-secondary"
          backgroundColor="#8A33FD
"
          text="Open second modal"
          onClick={this.openSecondModal}
        />

        {this.state.isFirstModal && (
          <Modal
            header="Product Delete!"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutside={this.handleOutside}
            text="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
            actions={
              <div className="button-container">
                  <Button 
                  className="btn" 
                  onClick={this.closeModal} 
                  text="No, Cancel"
                  />
                  <Button 
                  className="btn" 
                  onClick={this.closeModal}
                  text="Yes, Delete"
                  />
              </div>
            }
          />
        )}

        {this.state.isSecondModal && (
          <Modal_secondary
            header="Add Product “NAME”"
            closeButton={true}
            closeModal={this.closeModal}
            handleOutside={this.handleOutside}
            text="Description for you product"
            actions={
              <div className="button-container">
                <Button 
                    optionalСlassName="btn-only"
                  backgroundColor="#8A33FD"
                  onClick={this.closeModal}
                  text="ADD TO FAVORITE"
                />


              </div>
            }
          />
        )}
      </div>
    );
  }
}

