import React from "react";
import "./Modal_secondary.scss";

export default class Modal_secondary extends React.Component {

    render() {
        const { header, closeButton, text, actions, closeModal, handleOutside, pathImage, descrRaiting } = this.props;
        return (
            <div className="modal-wrapper-secondary" onClick={handleOutside}>
                <div className="modal-secondary">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h2>{header}</h2>
                            {closeButton && (
                                <span className="modal-close" onClick={closeModal}>
                            &times;
                        </span>
                            )}
                        </div>
                       
                        <div className="modal-body">

                            <p className="body-text">{text}</p>
                        </div>
                        <div className="modal-footer">{actions}</div>
                    </div>
                </div>
            </div>);
    }
}